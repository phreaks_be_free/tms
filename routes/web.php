<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
use App\User;

Route::get('/', 'HomeController@index')->name('home');

/* 
* -------------------API ACCESS TOKEN GUIDE--------------------------
* run php artisan passport::install in terminal and get a personal access client id
* add personal client id in AuthServiceProvider / Passport::personalAccessClientId();
* uncomment the below code to generate a api access token on /token
* 
*/
// Route::get('/token', function () {
//     $token = auth()->user()->createToken('user')->accessToken;
//     return $token;
// })->name('home');

/*
############################<3
* CONGO, you can now trigger the api and get all users in json at root/api/data
* checkout the api file
*                                                  <3#############################
*/




// Authentication routes
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::resource('timesheets', 'TimesheetController', ['except' => ['show']])->middleware('auth');

// supervisor routes
Route::get('/supervisor-dashboard', function () {
    $supervisors = User::where('role', 'supervisor')->get();
    $users = User::where('role', '!=', 'supervisor')->get();
    return view('supervisor-dashboard', ["users" => $users, "supervisors" => $supervisors]);
})->middleware('role:supervisor')->name('supervisor');

// employee routes
Route::get('/employee-dashboard', function () {
    $timesheets = auth()->user()->timesheets;
    return view('employee-dashboard', compact('timesheets'));
})->middleware('role:employee')->name('employee');

Route::get('/home', 'HomeController@index')->name('home');
