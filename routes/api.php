<?php

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->group(function () {
    Route::get('data', function (Request $request) {
        $users = User::get();
        if(empty($users)) {
            return response()->json([
                'success' => false,
                'message' => 'Nothing found in the database'
            ], 400);
        }
        $users = UserResource::collection($users);
        return response()->json([
            'success' => true,
            'data' => $users
        ], 200);
    });
});
