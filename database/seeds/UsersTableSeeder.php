<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'Ali',
                'role' => 'employee',
                'email' => 'artanha7@gmail.com',
                'password' => Hash::make('secret')
            ]
            );
        DB::table('users')->insert(
            [
                'name' => 'Jason',
                'role' => 'supervisor',
                'email' => 'foo@bar.com',
                'password' => Hash::make('secret')
            ]
            );
    }
}
