@extends('layouts.app') 
@section('content')
<div class="page-header">
    <h1>Employee Dashboard</h1>
</div>
<div class="row justify-content-center">
    <div class="col-md-12">
        @if(count($timesheets))
        <a class="btn btn-dark" data-toggle="collapse" href="#collapse">Collapse Table</a> @endif
        <a href="/timesheets/create" class="btn btn-primary">Create Timesheet</a> 
        @if(count($timesheets))
        <div class="collapse show" id="collapse">
            <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">date</th>
                        <th scope="col">time_from</th>
                        <th scope="col">time_to</th>
                        <th scope="col">comments</th>
                        <th scope="col">date_submitted</th>
                        <th scope="col">actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($timesheets as $ts)
                    <tr class="timesheet-row">
                        <td>{{ $ts->date }}</td>
                        <td>{{ $ts->time_from }}</td>
                        <td>{{ $ts->time_to }}</td>
                        <td>{{ $ts->comments }}</td>
                        <td>{{ $ts->created_at }}</td>
                        <td>
                            <div style="display: none" class="timesheet-actions form-inline justify-content-center">
                                <a style="color:black" title="Edit Timesheet" href="/timesheets/{{ $ts->id }}/edit" class=""><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <form action="/timesheets/{{ $ts->id }}" method="post">
                                    @csrf @method('DELETE')
                                    <button style="color:black" title="Delete Timesheet" class="btn btn-link" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</div>
@endsection