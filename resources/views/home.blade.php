@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(!empty(Auth::user()->role))
                    <h3>Hello {{ Auth::user()->name }}, your role is '{{ Auth::user()->role }}'!</h3>
                    <a class="btn btn-dark" href="{{ Auth::user()->checkRole('supervisor') ? route('supervisor') : route('employee') }}">View Dashboard</a>
                    @else
                    <h3>Hello {{ Auth::user()->name }}!</h3>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
@endsection
