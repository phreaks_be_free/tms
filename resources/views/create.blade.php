@extends('layouts.app') 
@section('content')
<div class="row justify-content-center">

    <div class="col-md-7">
        <form action="/timesheets" method="post">
            @csrf
            <div class="form-group">
                <i class="fa fa-calendar"></i>
                <label for="date">Date</label>
                <input value="{{ old('date') }}" required name="date" type="date" id="date" class="form-control">
            </div>
            <div class="form-group">
                <i class="fa fa-clock-o"></i>
                <label for="time_from">Time From (HH:MM)</label>
                <input value="{{ old('time_from') }}" required name="time_from" type="time" step="1" id="time_from" class="form-control">
            </div>
            <div class="form-group">
                <i class="fa fa-clock-o"></i>
                <label for="time_from">Time To (HH:MM)</label>
                <input value="{{ old('time_to') }}" name="time_to" type="time" step="1" id="time_from" class="form-control">
            </div>
            <div class="form-group">
                <i class="fa fa-comments"></i>
                <label for="coments">Comments</label>
                <input value="{{ old('comments') }}" name="comments" type="text" id="comments" class="form-control">
            </div>
            <input value="Create Timesheet" type="submit" class="btn btn-primary form-control">
        </form>
    </div>
</div>
@endsection