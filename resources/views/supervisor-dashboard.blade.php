@extends('layouts.app') 
@section('content')
<div class="page-header">
    <h1>Supervisor Dashboard</h1>
</div>
<div class="row justify-content-center">
    <div class="col-md-12">
        <a href="/timesheets/create" class="btn btn-primary btn-lg">Create Timesheet</a>

        <a title="Click to toggle data" class="btn btn-dark btn-lg" data-toggle="collapse" href="#supervisor">
                    Supervisors
                </a>
        <div class="collapse show" id="supervisor">
            @if (!empty($supervisors))
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th>username</th>
                        <th>email</th>
                        <th>timesheets</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($supervisors as $supervisor)
                    <tr id="supervisor-tr" title="{{ count($supervisor->timesheets) ? " Click to toggle data " : null}}" data-toggle="collapse"
                        data-target="#accordion-{{ $supervisor->id }}" class="clickable">
                        <td><i class="{{ !count($supervisor->timesheets) ? 'fa-disabled' : null }} fa fa-arrow-circle-right"
                                aria-hidden="true"></i></td>
                        <td>{{ $supervisor->name }} @if(\Auth::user()->id == $supervisor->id) {{ " (you)"}} @endif
                        </td>
                        <td>{{ $supervisor->email }}</td>
                        <td>{{ count($supervisor->timesheets )}}</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="accordion-{{ $supervisor->id }}" class="collapse">
                                @if(count($supervisor->timesheets))
                                <table class="table table-striped table-hover table-bordered">
                                    <thead class="">
                                        <tr>
                                            <th scope="col">date</th>
                                            <th scope="col">time_from</th>
                                            <th scope="col">time_to</th>
                                            <th scope="col">comments</th>
                                            <th scope="col">date_submitted</th>
                                            <th scope="col">actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($supervisor->timesheets as $ts)
                                        <tr class="timesheet-row">
                                            <td>{{ $ts->date }}</td>
                                            <td>{{ $ts->time_from }}</td>
                                            <td>{{ $ts->time_to }}</td>
                                            <td>{{ $ts->comments }}</td>
                                            <td>{{ $ts->created_at }}</td>
                                            <td>
                                                <div style="display: none" class="timesheet-actions form-inline justify-content-center">
                                                    <a style="color:black" title="Edit Timesheet" href="/timesheets/{{ $ts->id }}/edit" class=""><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <form action="/timesheets/{{ $ts->id }}" method="post">
                                                        @csrf @method('DELETE')
                                                        <button style="color:black" title="Delete Timesheet" class="btn btn-link" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <a title="Click to toggle data" class="btn btn-dark btn-lg" data-toggle="collapse" href="#other-users">
                Other Users
            </a>
        <div class="collapse show" id="other-users">

            @if (!empty($users))
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th>username</th>
                        <th>email</th>
                        <th>role</th>
                        <th>timesheets</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr id="user-tr" title="{{ count($user->timesheets) ? " Click to toggle data " : null}}" data-toggle="collapse" data-target="#accordion-{{ $user->id }}"
                        class="clickable">
                        <td><i class="{{ !count($user->timesheets) ? 'fa-disabled' : null }} fa fa-arrow-circle-right" aria-hidden="true"></i></td>
                        <td>{{ $user->name }}
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role }}</td>
                        <td>{{ count($user->timesheets )}}</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="accordion-{{ $user->id }}" class="collapse">
                                @if(count($user->timesheets))
                                <table class="table table-striped table-hover table-bordered">
                                    <thead class="">
                                        <tr>
                                            <th scope="col">date</th>
                                            <th scope="col">time_from</th>
                                            <th scope="col">time_to</th>
                                            <th scope="col">comments</th>
                                            <th scope="col">date_submitted</th>
                                            <th scope="col">actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($user->timesheets as $ts)
                                        <tr class="timesheet-row">
                                            <td>{{ $ts->date }}</td>
                                            <td>{{ $ts->time_from }}</td>
                                            <td>{{ $ts->time_to }}</td>
                                            <td>{{ $ts->comments }}</td>
                                            <td>{{ $ts->created_at }}</td>
                                            <td>
                                                <div style="display: none" class="timesheet-actions form-inline justify-content-center">
                                                    <a style="color:black" title="Edit Timesheet" href="/timesheets/{{ $ts->id }}/edit" class=""><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <form action="/timesheets/{{ $ts->id }}" method="post">
                                                        @csrf @method('DELETE')
                                                        <button style="color:black" title="Delete Timesheet" class="btn btn-link" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>

    </div>
</div>
@endsection