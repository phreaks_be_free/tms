@extends('layouts.app') 
@section('content')
<div class="row justify-content-center">

  <div class="col-md-7">
    <form action="/timesheets/{{ $timesheet->id }}" method="post">
      @csrf @method('PATCH')
      <div class="form-group">
        <i class="fa fa-calendar"></i>
        <label for="date">Date</label>
        <input required value="{{ old('date', $timesheet->date)}}" name="date" type="date" id="date" class="form-control">
      </div>
      <div class="form-group">
        <i class="fa fa-clock"></i>
        <label for="time_from">Time From (HH:MM)</label>
        <input required value="{{ old('time_from', $timesheet->time_from) }}" name="time_from" type="time" step="1" id="time_from" class="form-control">
      </div>
      <div class="form-group">
        <i class="fa fa-clock-text"></i>
        <label for="time_from">Time To (HH:MM)</label>
        <input required value="{{ old('time_to', $timesheet->time_to) }}" name="time_to" type="time" step="1" id="time_from" class="form-control">
      </div>
      <div class="form-group">
        <i class="fa fa-comments"></i>
        <label for="coments">Comments</label>
        <input value="{{old('comments', $timesheet->comments) }}" name="comments" type="text" id="comments" class="form-control">
      </div>
      <input value="Submit" type="submit" class="btn btn-primary form-control">
    </form>
  </div>
</div>
@endsection