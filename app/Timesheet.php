<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timesheet extends Model
{
    protected $fillable = [
        'user_id', 'date', 'time_from', 'time_to', 'comments', 'date_submitted'       
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
