<?php

namespace App\Policies;

use App\Timesheet;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TimesheetPolicy
{
    use HandlesAuthorization;

    public function change(User $user, Timesheet $timesheet)
    {
        return $timesheet->user_id == $user->id;
    }
    public function store(User $user) {
        return $user->checkRole('employee');
    }
}
