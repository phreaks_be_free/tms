<?php

namespace App\Http\Controllers;

use App\Timesheet;
use Illuminate\Http\Request;
use App\Policies\TimesheetPolicy;

class TimesheetController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:change,timesheet')->except('create', 'store', 'index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('store', Timesheet::class);
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('store', Timesheet::class);
        $attributes = $request->validate(
            [
                'date' => 'required|date',
                'time_from' => 'required|date_format:H:i:s',
                'time_to' => 'nullable|date_format:H:i:s|after:time_from',
                'comments' => 'nullable|string|min:3|max:255',
            ]
        );
        $attributes['user_id'] = auth()->user()->id;
        Timesheet::create($attributes);
        return redirect(auth()->user()->role . '-dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function show(Timesheet $timesheet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function edit(Timesheet $timesheet)
    {
        return view('edit', compact('timesheet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timesheet $timesheet)
    {
        $attributes = $request->validate(
            [
                'date' => 'required|date',
                'time_from' => 'required|date_format:H:i:s',
                'time_to' => 'required|date_format:H:i:s|after:time_from',
                'comments' => 'nullable|string|min:3|max:255',
            ]
        );
        $timesheet->update($attributes);
        return redirect()->route('employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timesheet $timesheet)
    {
        $timesheet->delete();
        return back();
    }
}
